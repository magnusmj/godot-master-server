"use strict";

const RelayTypes = {
	HostClientRelay: 1,
	NodeRelay: 2
}

const MessageTypes = {
	Init: 0, 
  Connected: 1,
  Disconnected: 2,
  Kick: 3,
  MessageTo: 4,
  MessageAll: 5,
  SetRelayData: 6
}

const RelayNodeTypes = {
	Node: 0,
	Host: 1,
	Client: 2
}

export { RelayTypes, MessageTypes, RelayNodeTypes }
