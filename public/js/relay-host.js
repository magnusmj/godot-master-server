"use strict";
import RelayBase from "./relay-base.js";
import { RelayTypes, MessageTypes } from "./relay-shared.js";

class RelayHost extends RelayBase {
	constructor(url, password){
		super(url, RelayTypes.HostClientRelay, password);		
		this.addEventListener("messageto", this.#onMessageTo);
		this.decoder = new TextDecoder();
	}
	#onMessageTo(e){
		console.log("onMessageTo");
		console.log(e);
		console.log(this.decoder.decode(e.msg));
	} 
}

export default RelayHost;
