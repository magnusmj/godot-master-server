"use strict";
import EventEmitter from 'events';
import bcrypt from 'bcryptjs';
import { WebSocketServer } from 'ws';
import { uuid } from './misc.js';
import { RelayTypes, MessageTypes, RelayNodeTypes } from "./public/js/relay-shared.js";

class RelaySocket {
	constructor(socket,id){
		this.id = id;
		this.uuid = uuid();
		this.socket = socket;
		socket.id = id;
	}
	send(msg){
		this.socket.send(msg);
	}
}
  
class Relay extends EventEmitter {
  constructor(hostSocket, initData) {
    super();  
  	this.seats = new Set([...Array(255).keys()]);
    this.sockets = [];
    this.id = uuid();
    this.type = initData.relayType;
    this.data = initData.data
    this.onMessage = this.onMessage.bind(this);
    this.onClose = this.onClose.bind(this);    
    this.connectWebsocket(hostSocket);
  }
  connectWebsocket(websocket){
  	const id = this.seats.popFirst();
  	if (id == undefined) return onConnectionDecliend(websocket);
  	const relaySocket = new RelaySocket(websocket, id);
    this.sockets[id] = relaySocket;
    websocket.addEventListener("message", this.onMessage);
    websocket.addEventListener("close", this.onClose);
    this.onConnectionAccepted(relaySocket);
  }
  onConnectionDecliend(websocket){
  	websocket.close();
  }
  onConnectionAccepted(relaySocket) {
  	const initMessage = {
  		type: MessageTypes.Init,
  		info: "Relay Connection Accepted", 
  		relayId: this.id,
  		relaySocket: {
  			id: relaySocket.id,
  			uuid: relaySocket.uuid,
  		},
  		nodeType: RelayNodeTypes.Node,
  		nodes: []
  	}
  	for (let id in this.sockets){
			const s = this.sockets[id];
			if (s.id != relaySocket.id) initMessage.nodes.push(s.id);
		} 
  	if (this.type == RelayTypes.HostClientRelay){
			if (relaySocket.id == 0){
				initMessage.nodeType = RelayNodeTypes.Host;
			} else {
				initMessage.nodeType = RelayNodeTypes.Client;	
			}					
  	} else {
  		initMessage.nodeType = RelayNodeTypes.Node
  	}
  	let bytes = new Uint8Array(Buffer.from("    " + JSON.stringify(initMessage)));
  	bytes[0] = MessageTypes.Init;
    relaySocket.send(bytes);
		bytes = new Uint8Array(Buffer.from("    "));
		bytes[0] = MessageTypes.Connected;
		bytes[1] = relaySocket.id;
		this.sendAll(bytes);
  } 
  onClose (e) {
		const websocket = e.target;
		delete this.sockets[websocket.id];
		this.seats.add(websocket.id);
    websocket.removeEventListener("message", this.onMessage);
    websocket.removeEventListener("close", this.onClose);    
    if (Object.keys(this.sockets).length <= 0){      
      this.emit("empty", this);
    }
    let bytes = new Uint8Array(Buffer.from("    "));
    bytes[0] = MessageTypes.Disconnected;
		bytes[1] = websocket.id;
		this.sendAll(bytes);
  }
  sendTrusted(bytes){
  	if (this.type == RelayTypes.HostClientRelay){
			const s = this.sockets[0];
			if (s == undefined) return;
			s.send(bytes);
		} else {
			sendAll(bytes)
		}
  }
  sendAll(bytes){
  	for (let id in this.sockets){
			const s = this.sockets[id];
			s.send(bytes);
		}
  }
  
  onMessage(e) {
  	//console.log(e.data);
	  const sender = e.target;
	  if (e.data.length < 2) {
	  	console.log("message not accepted");
		  return;
	  }
    try {
	    const bytes = new Uint8Array(e.data);
	    const type = bytes[0];
	    switch (type) {
	    	case MessageTypes.MessageTo:
	    		this.handleMessageTo(sender, bytes);
		    	break;
		    case MessageTypes.MessageAll:
		    	this.handleMessageAll(sender, bytes);
		    	break;
		    case MessageTypes.Kick:
		    	this.handleKick(sender, bytes);
		    	break;
		    case MessageTypes.SetRelayData:
		    	this.handleSetRelayData(sender, bytes);
		    	break;
	    }
    	
    } catch (e) {
      console.log(e);
    }
  }
  handleMessageTo(sender, bytes){
  	const target = bytes[1];
  	if (this.type == RelayTypes.HostClientRelay && sender.id != 0) {
			bytes[2] = sender.id;
  		//console.log("relay through host");
  		this.sockets[0].send(bytes);
  		return;
  	} 		
  	if (this.type != RelayTypes.HostClientRelay) bytes[2] = sender.id;
  	if (target in this.sockets){
  		let receiver = this.sockets[target];
  		receiver.send(bytes);
  	} //else console.log("not found: " + target); 
  }
  handleMessageAll(sender, bytes){
	  console.log("handleMessageAll");
  	if (this.type == RelayTypes.HostClientRelay && sender.id != 0) {
	  	bytes[2] = sender.id;
  		this.sockets[0].send(bytes);
  		return;
  	} 		
  	if (this.type != RelayTypes.HostClientRelay) bytes[2] = sender.id;
  	for (let id in this.sockets){
  		const s = this.sockets[id];
  		if (s.socket == sender) continue;
  		s.send(bytes);
  	}
  }
  handleKick(sender, bytes){
	  console.log("handleKick");
	  if (this.type == RelayTypes.HostClientRelay && sender.id != 0) return console.log("not allowed");
	  const target_id = bytes[1];
		if (this.type == RelayTypes.HostClientRelay && sender.id != 0) {
	  	bytes[2] = sender.id;
  		this.sockets[0].send(bytes);
  		return;
  	} else {
			if (target_id in this.sockets){
				let target = this.sockets[target_id];
				target.send(bytes);
				target.close();
			}
  	}
  }
  handleSetRelayData(sender, bytes){
  	if (this.type == RelayTypes.HostClientRelay && sender.id != 0) return console.log("not allowed");
  	let data = new TextDecoder().decode(bytes.subarray(4, bytes.length));  	
  	this.data = JSON.parse(data);
  }
  serialize(){
  	return {
  		id: this.id,
  		type: this.type,
  		data: this.data
  	} 
  }
}

export { Relay } 
