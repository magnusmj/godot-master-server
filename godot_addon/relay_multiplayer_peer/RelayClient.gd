extends RelayBase

const password = "default-password"

func _ready():
	super()
	connect(opened.get_name(), _on_open)
	connect(closed.get_name(), _on_close)
	connect(message_to.get_name(), _on_message_to)
	connect(connected.get_name(), _on_connected)

func _on_open():
	print("_on_open")
	var initData = { 
		"password": password, 
		"relayType": RelayTypes.HostClientRelay,
		"nodeType": RelayNodeTypes.Client,
		"id": relay_id
	}
	#print(JSON.stringify(initData))
	socket.send_text(JSON.stringify(initData))

func _on_close(code):
	print("Relay closed with code: %d. Clean: %s" % [code, code != -1])

func _on_message_to(event):
	print("client _on_message_to: " + str(self.relay_socket.id))
	print(event.data.get_string_from_utf8())

func _on_connected(peer_id):
	print("peer connected: " + str(peer_id))
