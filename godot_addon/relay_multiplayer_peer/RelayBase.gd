extends WebsocketBase

class_name RelayBase

enum RelayTypes {
	HostClientRelay= 1,
	NodeRelay= 2
}

enum RelayNodeTypes {
	Node = 0,
	Host = 1,
	Client = 2
}

enum MessageTypes {
	Init = 0,
	Connected= 1,
	Disconnected= 2,
	Kick= 3,
	MessageTo= 4,
	MessageAll= 5,
	SetRelayData= 6
}

var json = JSON.new()
var relay_id = ""
var relay_socket = { "id": -1 }

signal message_to(event)
signal message_all(event)
signal kicked(event)
signal connected(event)
signal disconnected(event)
signal relay_connected(event)

func _ready():
	super()
	init()

func init():
	message_received.connect(_on_message)

func _on_message(bytes: PackedByteArray):
	#print("Relay base _on_message")
	var message_type = bytes.decode_u8(0)
	match message_type:
		MessageTypes.Connected:
			_handle_connected_message(bytes)
		MessageTypes.Disconnected:
			_handle_disconnected_message(bytes)
		MessageTypes.Init:
			_handle_init_message(bytes)
		MessageTypes.MessageTo:
			_handle_message_to(bytes)
		MessageTypes.MessageAll:
			_handle_message_all(bytes)
		MessageTypes.Kick:
			_handle_kick(bytes)

func _handle_connected_message(bytes: PackedByteArray):
	emit_signal(connected.get_name(), bytes.decode_u8(1))

func _handle_disconnected_message(bytes: PackedByteArray):
	emit_signal(disconnected.get_name(), bytes.decode_u8(1))
	
func _handle_init_message(bytes: PackedByteArray):
	#print("handle init Message")
	bytes = bytes.slice(4)
	var text = bytes.get_string_from_utf8()
	if json.parse(text) == OK:
		var connectedMessage = json.data
		relay_id = connectedMessage.relayId
		relay_socket = connectedMessage.relaySocket
		#print(text)
		emit_signal(relay_connected.get_name(), connectedMessage)
  
func _handle_message_to(bytes: PackedByteArray):
	
	var e = {
		"bytes": bytes,
		"from": bytes.decode_u8(2),
		"target": bytes.decode_u8(1),
		"data": bytes.slice(4)
	}
	emit_signal(message_to.get_name(), e)

func _handle_message_all(bytes):
	var e = {
		"bytes": bytes,
		"from": bytes.decode_u8(2),
		"data": bytes.slice(4)
	}
	emit_signal(message_all.get_name(), e)

func _handle_kick(bytes):
	pass
 
func send_init(init_data):
	socket.send_text(JSON.stringify(init_data))

func send_to(target: int, bytes: PackedByteArray):
	bytes.insert(0,0)
	bytes.insert(0,0)
	bytes.insert(0,target)
	bytes.insert(0,MessageTypes.MessageTo)
	socket.send(bytes)

func send_all(bytes):
	bytes.insert(0,0)
	bytes.insert(0,0)
	bytes.insert(0,0)
	bytes.insert(0,MessageTypes.MessageAll)
	socket.send(bytes)

func kick_peer(peer_id):
	pass
