extends MultiplayerPeerExtension

class_name RelayMultiplayerPeer

var target_peer: int
var relay_base: RelayBase# = WebsocketBase.new()
var is_server = false
var max_clients = 255
var password = ""
var packets = []

signal relay_connected(event)

func create_client(url, id, password):
	self.password = password
	relay_base = RelayBase.new()
	relay_base.init()
	relay_base.url = url
	relay_base.relay_id = id	
	relay_base.opened.connect(_on_open_client)
	relay_base.closed.connect(_on_close)
	relay_base.message_to.connect(_on_message_to)
	relay_base.connected.connect(_on_connected)
	relay_base.disconnected.connect(_on_disconnected)
	relay_base.relay_connected.connect(_on_relay_connected)
	relay_base.connect_socket()

func create_server(url, max_clients, password):
	self.max_clients = max_clients
	self.password = password
	relay_base = RelayBase.new()
	relay_base.init()
	relay_base.url = url
	relay_base.opened.connect(_on_open_server)
	relay_base.closed.connect(_on_close)
	relay_base.message_to.connect(_on_message_to)
	relay_base.connected.connect(_on_connected)
	relay_base.disconnected.connect(_on_disconnected)
	relay_base.relay_connected.connect(_on_relay_connected)
	is_server = true
	relay_base.connect_socket()

func _on_open_client():
	print("_on_open_client")
	var initData = { 
		"password": password, 
		"relayType": RelayBase.RelayTypes.HostClientRelay,
		"nodeType": RelayBase.RelayNodeTypes.Client,
		"id": relay_base.relay_id
	}	
	relay_base.send_init(initData)

func _on_open_server():
	print("_on_open_server")
	var initData = { 
		"password": password, 
		"relayType": RelayBase.RelayTypes.HostClientRelay,
		"nodeType": RelayBase.RelayNodeTypes.Host,
		"data": { "name": "My Game" }
	}
	relay_base.send_init(initData)

func _on_relay_connected(e):	
	if not is_server:
		for node in e.nodes:
			#print("node: " + str(node))
			peer_connected.emit(node+1)
	emit_signal(relay_connected.get_name(), e)

func _on_connected(peer_id):
	print("_on_connected peer_id : "  + str(peer_id+1))
	if (peer_id != 0):
		emit_signal(peer_connected.get_name(), peer_id+1)

func _on_disconnected(peer_id):
	print("_on_disconnected peer_id : "  + str(peer_id+1))
	emit_signal(peer_disconnected.get_name(), peer_id+1)

func _on_close():
	pass

func _on_message_to(event):
	packets.push_back(event)

func _close():
	relay_base.close()

func _disconnect_peer(p_peer, p_force):
	relay_base.kick_peer(p_peer-1)
	
func _get_available_packet_count():
	return packets.size()

func _get_connection_status():
	match relay_base.current_state:
		WebSocketPeer.STATE_CLOSED:
			return ConnectionStatus.CONNECTION_DISCONNECTED
		WebSocketPeer.STATE_CONNECTING:
			return ConnectionStatus.CONNECTION_CONNECTING
		WebSocketPeer.STATE_OPEN:
			return ConnectionStatus.CONNECTION_CONNECTED
	return ConnectionStatus.CONNECTION_DISCONNECTED
	
func _get_max_packet_size():
	return 2e8

func _get_packet_script():
	var p = packets.pop_front()
	return p.data
	
func _get_packet_channel():
	return 0

func _get_packet_mode():
	return TransferMode.TRANSFER_MODE_RELIABLE
	
func _get_packet_peer():
	var p = packets[0]
	#print("sender: " +  str(p.from+1))
	return p.from+1

func _get_transfer_channel():
	return 0

func _get_unique_id():
	return relay_base.relay_socket.id + 1

func _is_refusing_new_connections():
	return false

func _is_server():
	return is_server

func _is_server_relay_supported():
	return true

func _poll():
	relay_base.poll()
	
func _put_packet_script(p_buffer):
	relay_base.send_to(target_peer-1, p_buffer)

func _set_refuse_new_connections(p_enable):
	pass

func _set_target_peer(p_peer):
	target_peer = p_peer

func _set_transfer_channel(p_channel):
	pass
	
func _set_transfer_mode(p_mode):
	pass
