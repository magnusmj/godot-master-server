 "use strict";
const bcrypt = require('bcryptjs');
const WebSocketServer = require('ws').Server;
const Misc = require('./misc.js');
const fs = require('fs');

let config = null;
let application = null;
let logs = null

function init(){
  let dir = './log';
  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
  fs.readFile('log/logs.json', 'utf8', (err, data) => {
    if (err) {
      logs = {}
    } else {
      try {
        logs = JSON.parse(data);
      } catch (e) {
        logs = {}
      }
    }
  });
}

function saveLogs() {
  fs.writeFile("log/logs.json", JSON.stringify(logs), function(err) {
    if(err) {
      console.log(err);
      //if (err) throw err;
    }
  });
}

function getFilename(id){
  let filename = null;
  if (id in logs){
    filename = logs[id].filename;
  } else {
    filename = Misc.uuid() + ".json";
    logs[id] = { filename: filename }
    saveLogs();
  }
  return filename;
}

function parseLogData(data){
  let log = {
    CpuName: data.CpuName,
    CpuCount: data.CpuCount,
    CpuFreq: data.CpuFreq,
    MemorySize: data.MemorySize,
    GpuName: data.GpuName,
    GpuType: data.GpuType,
    GpuMem: data.GpuMem,
    GpuSM: data.GpuSM,
    Os: data.Os,
    QualityLevel: data.QualityLevel,
    Version: data.Version,
    Resolution: data.Resolution,
    UserFeedback: data.UserFeedback
  }
  return log
}


function logAdd(req, res){
  //console.log(req.body);
  try {
    let data = req.body;
    let id = data.id;
    let password = data.password;
    let log = data.log;
    if (bcrypt.compareSync(password, config.passHash)){
      let filename = getFilename(id);
      fs.appendFile('log/' + filename, JSON.stringify(log) + "\n", function (err) {
        if (err) {
          console.log(err);
          res.writeHead(500, {'Content-Type': 'text/html'});
          return res.end("Server error.");
        } else {
          res.writeHead(200, {'Content-Type': 'text/html'});
          return res.end("Message recieved and stored.");
        }
      });
    } else {
      res.writeHead(401, {'Content-Type': 'html/text'});
      res.end("Access denied");
    }
  } catch (e) {
    res.writeHead(500, {'Content-Type': 'html/text'});
    res.end("Log entry not parsed");
  }

}

function logClear(req, res){
  //console.log(req.body);
  let data = JSON.parse(req.body);
  let id = data.id;
  let password = data.password;
  if (bcrypt.compareSync(password, config.passHash)){
    let filename = getFilename(id);
    fs.writeFile("log/" + filename, "", function(err) {
      if(err) {
        console.log(err);
        res.writeHead(500, {'Content-Type': 'text/html'});
        return res.end("Server error.");
      } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        return res.end("Log cleared.");
      }
    });
  } else {
    res.writeHead(401, {'Content-Type': 'html/text'});
    res.end("Access denied");
  }
}

function logGet(req, res){
  //console.log(req.query);
  try {
    let data =req.query;
    let id = data.id;
    let password = data.password;
    if (bcrypt.compareSync(password, config.passHash)){
      let filename = getFilename(id);
      fs.readFile('log/' + filename, 'utf8', (err, data) => {
        if (err) {
          res.writeHead(404, {'Content-Type': 'html/text'});
          return res.end("log not found");
        } else {
          let jsonObject = {}
          jsonObject.logs = []
          let datas = data.split("\n");
          for (let i in datas){
            try {
              jsonObject.logs.push(JSON.parse(datas[i]));
            } catch (e){}
          }
          res.writeHead(200, {'Content-Type': 'application/json'});
          return res.end(JSON.stringify(jsonObject));
        }
      });
    } else {
      res.writeHead(401, {'Content-Type': 'html/text'});
      res.end("Access denied");
    }
  } catch (e) {
    res.writeHead(500, {'Content-Type': 'html/text'});
    res.end("Log entry not parsed");
  }
}

module.exports = function(app, server, conf) {
  config = conf;
  app.post("/log/add", logAdd);
  app.post("/log/clear", logClear);
  app.get("/log/get", logGet);
  init();
}
