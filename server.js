"use strict"; 
import fs from 'fs';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import bcrypt from 'bcryptjs';
import masterServer from "./master-server.js";
import relays from "./relays.js";
//import Logger from "./logger.js"; // (app, server, config);

let configfile = "config.json";
let password = null;

for (let i in process.argv){
  let arg = process.argv[i];
  if (arg.includes("cfg=")  || arg.includes("config=") || arg.includes("configfile="))
    configfile = arg.split("=")[1];
  if (arg.includes("password=") || arg.includes("pw="))
    password = arg.split("=")[1];
}

let config = null;

try {
  if (fs.existsSync(configfile)){
    let data = fs.readFileSync(configfile, 'utf8')
    config = JSON.parse(data);  
  }
} catch (e){
  console.log('config file ' + configfile + ' not found');  
  console.log(e);
}

if (config == null) {
  console.log('please provide config file');
  process.exit(1);
}

if (password) config.passHash = bcrypt.hashSync(password, 10);

function startCallback(){
	console.log('Simple Master Server backend listening on port ' + config.port + '!')
}

const app = express();
const server = app.listen(config.port, startCallback);

app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('./public'))

masterServer(app, server, config);
relays(server, config); 
//logger(app, server, config);

