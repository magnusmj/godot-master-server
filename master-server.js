"use strict";
import bcrypt from 'bcryptjs';
import { uuid } from './misc.js';
import { getRelays } from "./relays.js";

let servers = {}
let config = {}

const MessageTypes = {
	Registered: 0,
	Error: 1,
	NotAuthenticated: 2,
	Updated: 3,
	Removed: 4
}

function parseServerData(data, hash){
  let server = {
   	hash: hash,
   	data: data,
    timeStamp: new Date()
  }
  return server
}

function auth(req, res, next) {
	console.log(req.body);
  try {
  	let password = req.body.password; 
		if (bcrypt.compareSync(password, config.passHash)){ 
			next();
		} else {
    	let response = { 
      	type: MessageTypes.NotAuthenticated,
      	message: "Not authenticated"
      }
      res.writeHead(401, {'Content-Type': 'application/json'});
      res.end(JSON.stringify(response));
    }		
	} catch (err){
    let response = { 
    	type: MessageTypes.Error,
    	message: err
    }
    res.writeHead(500, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(response));
  } 
}

function registerServer(req, res){
	try{
		let uniqueIdentifier = uuid();
		let hash = bcrypt.hashSync(uniqueIdentifier, 8);
		let server = parseServerData(req.body.data, hash);
		servers[uniqueIdentifier] = server;
		let response = { 
			type: MessageTypes.Registered, 
			server: server
		}
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(response));
	} catch(e){
	 	let response = { 
			type: MessageTypes.Error,
			message: e
		}
		res.writeHead(500, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(response));
	}
}

function updateServer(req, res){
	console.log(req.body);
}

function cleanServers(){
  let keys = Object.keys(servers);
  for (let i in keys){
    let id = keys[i];
    let server = servers[id];
    let now = new Date();
    if (now - server.timeStamp > 10000){
      delete servers[id];
    }
  }
}

function getServers(req, res){
  try {
    cleanServers();
  } catch (err){
    console.log(err);
  }
  res.writeHead(200, {'Content-Type': 'application/json'});
  let relays = getRelays();
  
  let serversData = {
  	servers: Object.values(servers),
  	relays: getRelays().serialize()
  }
  res.end(JSON.stringify(serversData));
}

function init(app, server, conf){
  config = conf;
  app.post("/servers", auth, registerServer);
  app.put("/servers/{id}", auth, updateServer);
  app.get("/servers", getServers);
}

export { init as default }


