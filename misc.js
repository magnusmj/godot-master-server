"use strict";

function uuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

Set.prototype.popFirst = function () {
  const firstItem = this.values().next().value; // Get the first item
  if (firstItem !== undefined) {
    this.delete(firstItem); // Remove the item from the Set
  }
  return firstItem; // Return the removed item
};

export { uuid }
